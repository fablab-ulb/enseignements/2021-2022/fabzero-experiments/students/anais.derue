# 5. Electronics 1 - Prototyping

This week we learned how to use an **Arduino**

## What's an arduino ?

An [arduino](https://www.arduino.cc/) is an open source electronic prototyping board. You can plug inputs into it, such as a button or a light sensor, and outputs, such as leds and buzzers. Then you open your IDE, and program it !
It's an easy way of prototyping many things, and there are tons of beginner-friendly tutorials on the internet.

![](../images/arduino.jpg)

## Security measures

Arduinos are very easy to use but there are still a few things to know in order not to burn them.

- When we write code in arduino (we'll dig into that later), we need to say if a pin is an input or an outut. It's important to pay attention not to switch them because that could cause problems.
- Some pins are groupped together. We can plug at max 3 pins per group.
- When we use an external source of power, we need to make sure not to apply more than 5V on an input
- when doing the wiering, we should always unplug the arduino from the computer to avoid risk of short circuit

## I/O

Arduinos can do many things because they have a lot of In/Out pins, so we can plug things into them. Mosto of these pins fall into one of two categories : the **analog** ones, and the **digital** ones. Analog pins can handle analog signal, i.e. signal that is continuous. On the other hand, digital pins only deal with digital dignal.

![](../images/Analog-vs-Digital.png)

Some pins have a # symbol. It means pwm, which stqnds for _Pulse Width Modulation_. It fakes an analog signal by using binary values. It can simulate a sin signal for example. It can be very useful to control lights or speakers.

![](../images/pwm.png)

### input examples

- switches
- detectors (humidity, sound,...) -> can be analog or digital

### output examples

- led
- displays
- electro-mechanical parts (may need an external power supply)

## Code

To make your arduino do cool stuff, you need to program it using the [Arduino IDE](https://www.arduino.cc/en/software)

To get started, yo can use the examples that are avaliable directly on the arduino IDE

![](../images/arduino_examples.png)

Those examples are always useful, even if you aren't a beginner ! You can use them to build bigger programs, without needing to reinvent the wheel every time yo need to use a button.

An arduino code always consists of two parts : the setup and the loop. Setup is for code that only need to be run once, and loop is for the main part of the code, it will run endlessly.

We use pinmode to set our inputs and outputs
We should always use variables to declare our pins, that way if we want to change the pin, we just need to modify one line.

Once the code is written, we compile it then upload it to the arduino (careful to select the right type of board and the right port) (here we use arduino uno boards)

## led blink

The first program we will upload is one that makes the led blink. To do it, I used the led_blink example, and changed it a little so it's an external led that blinks instead of the one that's on the arduino.

```arduino
int led = 2;
void setup() {
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);
  delay(1000);
  digitalWrite(led, LOW);
  delay(1000);
}
```

![](../images/arduino1.jpg)

## Fade

To make things a little more interesting, we now want our led to have a fade effect instead of just blinking. To do that, we connect the led to a pin that can do pmw. This code is purely an example avaliable on the arduino IDE.

```arduino
int led = 9;           // the PWM pin the LED is attached to
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup() {
  // declare pin 9 to be an output:
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // set the brightness of pin 9:
  analogWrite(led, brightness);

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}
```

## Light detector

In one of the captor boxes avaliable in the fablab, I found a light detector, so I decided I wanted to use it. For now I just want to print the values onto the monitor. There's an example that does just that, so I used the example code once again (there really is an example avaliable for all of the basic uses of the arduino, that makes it very easy for beginners)

The serial functions all deals with printing things onto the monitor.

- Serial.begin starts the communication
- Serial.print lets you print on the monitor
- Serial.println is used to go to a new line

For this use, we use an analog pin, as the values we recieve from the captor are analogic.

```arduino
int capteur_lum = 0; // capteur branché sur le port 0
int analog_lum; // valeur analogique envoyé par le capteur

void setup(void) {
  Serial.begin(9600); // démarrer la liaison série
}

void loop(void) {
  analog_lum = analogRead(capteur_lum); // lecture de la valeur analogique, qu'on enregistre dans analog_lum
  Serial.print("Valeur luminosité = ");
  Serial.print(analog_lum);
  Serial.println("");
  delay(1000);
}
```

![](../images/lum.png)

## Light detector + leds

I noticed while running the previous code that the values given by the light detector are usually between 900 and 1000 when I don't do anything, and between 600 and 700 when I put my finger on the captor to block the light. I decided to use this info for my final code. I used an 'If' in the loop part of my code to check the value returned by the captor. If it's under 800, I probably have my finger on it. I can now use the light detector as a touch button. If I touch the detector I light a red led, if I don't then I light a green led.

I noticed I need to be careful to turn off the led when I don't want it to be lit anymore, otherwise both my leds will stay on, even when the condition to be lit isn't met anymore.

```arduino
/*
    FILE   : lightled.ino

    AUTHOR : Anaïs Derue <anais.derue@ulb.be>

    DATE   : 2022-03-17

    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

*/
int capteur_lum = 0; // capteur branché sur le port 0
int analog_lum; // valeur analogique envoyé par le capteur
int red_led = 2;
int green_led = 4;


void setup(void) {
  Serial.begin(9600); // démarrer la liaison série
  pinMode(red_led, OUTPUT);
  pinMode(green_led, OUTPUT);


}

void loop(void) {
  analog_lum = analogRead(capteur_lum); // lecture de la valeur analogique, qu'on enregistre dans analog_lum
  Serial.print("Valeur luminosité = ");
  Serial.print(analog_lum);
  Serial.println("");
  delay(10);

 if(analog_lum < 800){
    digitalWrite(red_led, HIGH);
    digitalWrite(green_led, LOW);
 }
 else{
    digitalWrite(green_led, HIGH);
    digitalWrite(red_led, LOW);
 }
}
```

![](../images/arduino3.jpg)
![](../images/arduino4.jpg)
