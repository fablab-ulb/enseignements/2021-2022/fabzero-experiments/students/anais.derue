# 1. Documentation

This week we focused on how to document our journey through this course.

## Linux

I decided not to partition my computer because I don't like switching between Windows and Linux, and my love for video games is to big to abandon Windows. However I use WSL : Windows Subsystem for Linux.

> "Windows Subsystem for Linux (WSL) is a compatibility layer for running Linux binary executables (in ELF format) natively on Windows 10, Windows 11, and Windows Server 2019. " - Wikipedia

Basically it lets you use your linux command-line tools and apps inside a terminal (I use Windows terminal). The cool thing is that it requires fewer resources than a virtual machine, you have all your files at the same place and you have all the efficiency and simplicity of Linux.

![](../images/wsl.png)

## CLI

Now that we are in a terminal, we need to use the CLI (Command line Interface) in order to do things. What things ? Anything ! With the CLI you can navigate through your files, create new ones, run programs install softwares and much more.

Here are a few useful commands :

| Command     | Description             | Argument                                 |
| ----------- | ------------------------| -----------------------------------------|
| cd          | change directory        | full path or relative path               |
| mkdir       | create directory        | name of the directory you want to create |
| pwd         | print working directory |                                          |
| ls          | list                    |                                          |
| rm          | remove                  | name of the directory you want to delete |
| touch       | create                  | name of the file you want to create      | 

Small tips to save you some time :

    clear to clear the terminal
    tab to autocomplete the line
    ↑ and ↓ to cycle through previous commands
    . means current directory
    .. means parent of current directory

## Docs

It's now time to actually write something or we won't be able to document anything.
To do so I use Visual Studio Code, which is a powerful open source text editor. It is a familiar environment for me because that's what I use to code. I like it because it's very intuitive and highly customizable. It also lets you preview markdown as you write it. The image below shows markdown in the vs code editor, before formatting.

![](../images/vscode.png)

## Markdown

Markdown is a lightweight markup language. When you write a document in markdown, you use syntax elements to indicate how you want your text to look. Also unlike microsoft Word or google docs, you won't see the final look of your document as you write it, unless you have a tool to preview it.

![](../images/markdown.png)

There are a few syntax elements to learn in order to make markdown look nice. For example **#** is used to indicate titles. The number of # you use correpond to the level of your title.
Here are other useful elements :

| Element | Meaning | Render        |
| ------- | ------- | ------------- |
| \*      | italic  | _spaghetti_   |
| \*\*    | bold    | **hard rock** |

You can find more elements in the guide I linked down this page [^1]

## Git

The last thing to learn this week is how to use Git. First of all, what _is_ Git ?
Git is a is a free and open source distributed version control system. This means it is a tool that lets you save each version of a project. It is very useful as you are free to change anything you want on your code, and go back to prior versions if you broke something in the process. It is also very useful for working with other people, as it lets you see who changed what and why they did it.

In order to use Git, you first need to install it. You can do it using the command :

     sudo apt-get install git-all

Now That git is installed on your computer, you just need to learn a few commands to use it.

![](https://imgs.xkcd.com/comics/git.png)

Here are basic useful commands :

| Command    | Effect                                                                                                     |
| ---------- | ---------------------------------------------------------------------------------------------------------- |
| git pull   | get the lastest version of the project you're working on from the remote repository to your computer       |
| git add    | add the files you want to the stage <br /> if you want to add all the modified files, just type git add -A |
| git status | lets you see what files are one the stage before commiting                                                 |
| git commit | the most useful command. create a new version of your project with the changes you made                    |
| git push   | update the remote repository with your lastest commit                                                      |

When using git commit, you should always add a message describing what the changes does, like this :

        git commit -m "add cool feature"

## Resize images

We want to make our website as lightweight as possible. The easiest way to de so is to reduce the size of the images we use.

One of the ways to achieve this, is to use an online converter, such as [ResizeImage](https://resizeimage.net/), or an image editing software, such as [Gimp](https://www.gimp.org/). However I don't really like these options as they are slow and not very practical.
The way I like to do it is to resize my images using the command line, with [Image magick](https://imagemagick.org/index.php).

You can install Image magick using this line in the shell :

        sudo apt-get install imagemagick

And then resize your images this way :

        convert  -resize 20% source.png dest.jpg

This line will make your new image 20% its original size.

If you want to make your image a specific size, you can use the command like this :

        convert -resize 1024X768  source.png dest.jpg

If you want to save your resized image over the previous one, you can use the morgify command

        mogrify -resize 50% *.png

We can notice a huge difference just by making your image a jpg and make it 90% its original size :

         convert -resize 90% slack.png slack-small.jpg

![](../images/resize.jpg)

## Useful links

[^1]: [Markdown guide](https://www.markdownguide.org/cheat-sheet/)

[A visual introduction to Git](https://medium.com/@ashk3l/a-visual-introduction-to-git-9fdca5d3b43a)
