# 2. Computer-Aided Design (CAD)

This week we learned how to design objects using two softwares : OpenSCAD and FreeCAD. We will then be able to print those objects with 3d printers.

## OpenSCAD

OpenSCAD is a free open source software that lets you make 3d models using lines of code.

### Downloading OpenSCAD

Before starting to do anything, you first need to download OpenSCAD [here](https://www.openscad.org/downloads.html).

### How does it work ?

You can code basic shapes on the zone on the left on the screen, and then translate, turn and modify then however you want using functions [^1]

![](../images/openscad0.png)

## FreeCAD

FreeCAD is also an open source free software that lets you create 3d models, but instead of code is uses parametric modeling.

### Downloading FreeCAD

You can download FreeCAD here [here](https://www.freecadweb.org/downloads.php).

### How does it work ?

You can create objects by modifying simple 3d objects such as cubes and spheres.

![](../images/freecad0.png)

## FlexLink

Time to make something !

This week's objective is to create a FlexLinx using one of the two softwares, and make it parametric.

A [FlexLink](https://www.compliantmechanisms.byu.edu/flexlinks) is a [Compliant Mechanism](https://en.m.wikipedia.org/wiki/Compliant_mechanism), designed for lego bricks.

I will try to recreate the fixed-fixed beam :

![](../images/flexlink.png)

To do so, I will use OpenSCAD as it will be easier to make the flexlink parametric.

I will use 'modules' to break my object into simple parts that I will assemble together. I will also try my best not to hard-code any value, and use variables instead. This way, the code will be more understandable and maintainable.

My flexlink consists of two main parts : the end parts (the round rectangle with the two holes), and the middle part. To simplify the comprehension of my code, I will also add a 'hole' part, that is just a cylinder.

### the hole

The hole is the simpliest part in this project. I made it using the 'cylinder' function and two variables : one for the height and one for the radius.
This is the code for a hole :

```openscad
module hole(height, radius){
    cylinder(height, radius, radius, true);
}
```

### the middle part

The middle part is also very simple, it is a very long cube.

```openscad
module middle_piece(height, lenght, width){
    cube([lenght, width, height], true);
}
```

This is what it looks like :

![](../images/middlepart.png)


### the end part

The end part consists of a cube a bit streched with very round edges, and two holes. However, the simplest way of designing it is by using the 'hull' function on two cylinders. This creates the illusion of a rectangle with perfectly round edges.

I also use the 'difference' function to substract the two holes to the piece.

Here is the code :

```openscad
module end_piece(height, length, width){
    // difference between the piece and two holes
    difference(){
        // using hull makes it easier to have rounded corners
        hull(){
            // two cylinders joined together
            translate([length,0,0])cylinder(height, width, width, true);
            translate([-length,0,0])cylinder(height, width, width, true);
        }
        // two holes
        translate([hole_distance,0,0])hole(height, hole_radius);
        translate([-hole_distance,0,0])hole(height, hole_radius);
    }
}
```

This is what it looks like :

![](../images/endpart.png)

### the FlexLink

Now to make my full FlexLink, I just join my middle piece with two end pieces, and it's done ! The 'union' function assures the pieces are joined together.

```openscad
module flexlink() {
    union(){
        translate([shift,0,0])end_piece(height, length, width);
        translate([-shift,0,0])end_piece(height, length, width);
        middle_piece(height_middle, length_middle, width_middle);
    }
}
```

To make the render smooth, I use this line at the beginning of my code

```openscad
$fn=100;
```

## Full code and render

Here is the full code for my FlexLink including the values for the variables, as well as the final render :

## Code

```openscad

/*
    FILE   : flexlink.scad

    AUTHOR : Anaïs Derue <anais.derue@ulb.be>

    DATE   : 2022-02-22

    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

*/

$fn=100;

//varibles

height = 2;
width = 3;
length = 4;

height_middle = height;
width_middle = 1;
length_middle = 50;

hole_radius = 2;
shift = length_middle/2 + 6;
hole_distance = 3;

flexlink();

module hole(height, radius){
    cylinder(height, radius, radius, true);
}
module end_piece(height, length, width){
    // difference between the piece and two holes
    difference(){
        // using hull makes it easier to have rounded corners
        hull(){
            // two cylinders joined together
            translate([length,0,0])cylinder(height, width, width, true);
            translate([-length,0,0])cylinder(height, width, width, true);
        }
        // two holes
        translate([hole_distance,0,0])hole(height, hole_radius);
        translate([-hole_distance,0,0])hole(height, hole_radius);
    }
}

module middle_piece(height, lenght, width){
    cube([lenght, width, height], true);
}


module flexlink() {
    union(){
        translate([shift,0,0])end_piece(height, length, width);
        translate([-shift,0,0])end_piece(height, length, width);
        middle_piece(height_middle, length_middle, width_middle);
    }
}
```

### Render

This is what my flexlink looks like !

![](../images/openscad1.png)

You can find the [opendcad file](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/anais.derue/-/blob/main/files/flexlink_v0.scad) and the [stl file](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/anais.derue/-/blob/main/files/flexlink_v0.stl) on my gitlab !

### 3d Model

I used sketchfab to upload my flexlink design and embed a preview here :

<div class="sketchfab-embed-wrapper"> <iframe title="Flexlink Anaïs" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share src="https://sketchfab.com/models/b92dcf8493ac4c25be6edf199d3ac67c/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/flexlink-anais-b92dcf8493ac4c25be6edf199d3ac67c?utm_medium=embed&utm_campaign=share-popup&utm_content=b92dcf8493ac4c25be6edf199d3ac67c" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Flexlink Anaïs </a> by <a href="https://sketchfab.com/octopunk?utm_medium=embed&utm_campaign=share-popup&utm_content=b92dcf8493ac4c25be6edf199d3ac67c" target="_blank" style="font-weight: bold; color: #1CAAD9;"> octopunk </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=b92dcf8493ac4c25be6edf199d3ac67c" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

## Useful links

[^1] [OpenSCAD Cheatsheet](https://openscad.org/cheatsheet/)
