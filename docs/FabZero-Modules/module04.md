# 4. 3d printing

This week we dived into the world of 3d printing. We learned how to use a slicer and a 3d printer. We also learned about code licenses and how to use them.

## Using the slicer

When we have the [STL](<https://en.wikipedia.org/wiki/STL_(file_format)>) files of our object, we need to slice them in order to be able to print them. The slicer is a software that will take our 3d object and convert it into a ready-to-print code.

The slicer we will use is [PrusaSlicer](https://www.prusa3d.com/fr/page/prusaslicer_424/), because it is free, easy to use and widely used among the 3d printing community.

When you open your STL file on the slicer it will look like this
![](../images/slicer.png)

The panel on the right lets you pick the printer you want to use, and select adjust how you want to print your object.

The first line lets you pick the height of each layer of plastic. If you select 0.10mm, your print will be better quality but will take much longer to print. If you don't care about then select 0.3 as it will print faster. 0.2 is the basic setting and works fine for most cases.

The infill is here set to 20%. You can ajust it on how resistant you want your object to be. It is usually useless to go over 25-30%.

You can add a raft if you want to help your object to stick to the bed. This is particularly usefull if your piece is thin and don't cover much surface, as it will probably move without a raft.

You need to orient your piece in a way that will print best. You can do that by using the dedicated icon on the left of the screen.

If your piece still cannot be printed correctly (the slicer will alert you), you will need to add supports. Supports are pieces of plastic that will help the printer. They come off easily, but I always try not to use them. There are tricks to make your designs 3d-printing-friendly, such as making smooth angles instead of 90 degrees turns.
If you still need to add supports, you can either auto-generate them, or use the paint brush icon on the left, to select where you want supports.

Once everything is ready, you can click the button on the bottom-right corner of the screen. It will generate the gcode the printer needs to print your object.

![](../images/benchy.png)

Then you will get a few additional info about your print. Especially how long it will take to print.

## Materials

There are different materials that can be used to 3d print. The most common one is PLA, because it is cheap and works well with most prints. It melts at relatively low temperatures as well (180-210ºC). The second most common is PETG. PETG is more resistant to the sun, and outdoor conditions in general. It is also more flexible, durable, and avaliable in transparent colors. PETG prints at 220-260ºC, and is a bit more expensive.

## Torture test

To test what our printer is capable of, and to see if it is well calibrated, we need a reference print, a _torture test_.
The one I picked is very famous, it is called a **benchy**. Benchy is for benchmark, as it will test many propreties of your printer.

This is what the benchy looks like

![](../images/benchy2.png)

The benchy has overhanging surfaces, tiny details on its surface and ont the first layer, bridge parts, and many other things that will make it difficult to print.
You can take a closer look at those features [here](https://www.3dbenchy.com/features/)

And here it is, after it's done printing
![](../images/benchy3.jpg)

We can see that the printer is well-calibrated, as all the details are here and the finished product looks pretty good

## Licensing

When we share code on the internet, we should license it, that way other people will know how they are allowed to use it. A very used one is the [Creative Commons license](https://creativecommons.org/). You can easily choose which license you want [here](https://creativecommons.org/choose/).

When you use code under a CC-license, you should follow the license conditions.

One condition that all CC licenses have is attribution. When we attribute someone's work, we should mention :

- the title of the work
- the name of the author
- the source
- the license they used

On module 2, I published code for my FlexLink design.
This week, I added a license to this code. This is the part that was added, on top of my code

```openscad
/*
    FILE   : flexlink.scad

    AUTHOR : Anaïs Derue <anais.derue@ulb.be>

    DATE   : 2022-02-22

    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

*/
```

## Using other people's work

Now that we know how licenses work, we can use other people's work and license it correctly.

Our goal is to create a flexlink kit.

To make this kit, I used Nathalie's ovni flexlink

```openscad
/*
FILE   : ovni_final.scad

AUTHOR : Nathalie Wéron

DATE   : 2022-02-25
MODIFIED : 2022-03-24
MODIFIED : 2022-04-02

LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)..

//*/

$fn=50; //definition of roundness
h = 8; // heigth of the flexlinks
d = 90; // length of the middle (do not include the extremities)
w = 1; // thickness of the middle
rc=4; // radius for the extremities
e=8; // distance between the holes centres
hole = 2.5; // radius of the holes
//size of extremities = e + 2*rc
// total lenght = d + 2*size of extremities

difference(){
    union(){ //BODIES
        // middle
        translate([-w/2,e+rc,0])
        cube([w,d,h]);

        //extremity 1: top
        cylinder(h, r=rc);
        translate([0,e,0])
        cylinder(h, r=rc);
        translate([-rc,0,0])
        cube([2*rc,e,h]);

        //extremity 2: bottom
        translate([0,d+(2*rc+e),0]){
            cylinder(h, r=rc);
            translate([0,e,0])
            cylinder(h, r=rc);
            translate([-rc,0,0])
            cube([2*rc,e,h]);
        }

        //OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_body
            translate([0,-(3*e/2),0]){//put at centre
                cylinder(h, r=rc);
                translate([0,3*e,0])
                cylinder(h, r=rc);
                translate([-rc,0,0])
                cube([2*rc,3*e,h]);
            }

            //extremity1_body: left
            translate([0,-(e+e/2),0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,45]){
                    translate([-rc,-2*e,0])
                    cube([2*rc,2*e,h]);
                    translate([0,-2*e,0])
                    cylinder(h,r=rc);
                }
            }

            //extremity2_body: right
            translate([0,e+e/2,0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,-45]){
                    translate([-rc,0,0])
                    cube([2*rc,2*e,h]);
                    translate([0,2*e,0])
                    cylinder(h,r=rc);
                }
            }
        }
    }
    //HOLES

    //extremity 1_holes: top
    cylinder(h, r=hole); //hole 1
    translate([0,e,0]) // hole 2
    cylinder(h, r=hole);

    //extremity 2_holes: bottom
    translate([0,d+(2*rc+e),0]){
        cylinder(h, r=hole); //hole 1
        translate([0,e,0]) // hole 2
        cylinder(h, r=hole);
    }

    //HOLES OVNI
        translate([0,e+rc+d/2,0])
        rotate([0,0,90]){
            //middle_holes
            translate([0,-(e+e/2),0]){ //put at centre
                cylinder(h, r=hole); //left hole
                translate([0,e,0]) // middle hole
                cylinder(h, r=hole);
                translate([0,2*e,0])
                cylinder(h, r=hole);
                translate([-hole,e,0])
                cube([2*hole,e,h]);
                translate([0,3*e,0]) // right hole
                cylinder(h, r=hole);

            }

            //extremity1_holes: left
            translate([0,-(e+e/2),0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,45]){
                    translate([0,-e,0]) //second hole
                    cylinder(h,r=hole);
                    translate([0,-2*e,0]) //end hole
                    cylinder(h,r=hole);
                }
            }

            //extremity2_holes: right
            translate([0,e+e/2,0]){ //fix the futur centre at the origin before the rotation
                rotate([0,0,-45]){
                    translate([0,e,-1]) //second hole
                    cylinder(h+2,r=hole);
                    translate([0,2*e,-1]) //end hole
                    cylinder(h+2,r=hole);
                }
            }
        }
}
```

![](../images/ovni.png)

And I also used Benjamin's work and modified it to create a new piece, that makes a 'X' shape. [Here](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/benjamin.hainaut/FabZero-Modules/module02/) you can see how his code works.
I used the grid he provided to create my flexlink.

Benjamin did not include a license in his code, so I just credited him but I'm unsure about how I'm allowed to use it.

![](../images/flexmodel.png)

```

/*

FILE : xflex.scad

AUTHOR : Anaïs Derue

DATE OF MODIFICATION : 07/04/2022

FROM : Benjamin HAINAUT (https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/benjamin.hainaut/FabZero-Modules/module02/)

*/

$fn = 100;

hri = 2;//Hole Radius In
hro = 4;//Hole Radius Out
hd = 8;//Hole distance
height = 10;

gs = 10;//Grid Size

module brick(list = [1]){
    difference(){
        hull(){
            for(i=[0:1:gs*gs-1]){
                if(len(search(i, list,0)) >= 1){
                    translate([i%gs*hd,floor(i/gs)*hd,0])cylinder(h=height,r=hro,center=true);
                }
            }
        }
        for(i=[0:1:gs*gs-1]){
                if(len(search(i, list,0)) >= 1){
                    translate([i%gs*hd,floor(i/gs)*hd,0])cylinder(h=height,r=hri,center=true);
                }
            }
    }
}
module link(A=0,B=35){
    length = sqrt(((B-A)%gs*hd-hro)^2+(floor((B-A)/gs)*hd-hro)^2);
    angle = atan(floor((B-A)/gs)/((B-A)%gs));
    translate([A%gs*hd+hri,floor(A/gs)*hd-0.5+hri,-5])rotate([0,0,angle])cube([length,1,height]);
}
union(){
    brick([40,31, 22, 13, 4]);
    brick([95,86,77,68,59]);
    link(40,59);
    link(4, 95);
}
```

And here is the final result

![](../images/xflex.png)

Now my kit is complete and ready to print !

![](../images/kit.png)
