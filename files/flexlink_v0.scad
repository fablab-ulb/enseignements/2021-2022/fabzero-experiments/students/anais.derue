/*
    FILE   : flexlink.scad

    AUTHOR : Anaïs Derue <anais.derue@ulb.be>

    DATE   : 2022-02-22

    LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

*/

$fn=100;

//varibles

height = 2;
width = 3;
length = 4;

height_middle = height;
width_middle = 1;
length_middle = 50;

hole_radius = 2;
shift = length_middle/2 + 6;
hole_distance = 3;

flexlink();

module hole(height, radius){
    cylinder(height, radius, radius, true);
}
module end_piece(height, length, width){
    // difference between the piece and two holes
    difference(){
        // using hull makes it easier to have rounded corners
        hull(){
            // two cylinders joined together
            translate([length,0,0])cylinder(height, width, width, true);
            translate([-length,0,0])cylinder(height, width, width, true);
        }
        // two holes
        translate([hole_distance,0,0])hole(height, hole_radius);
        translate([-hole_distance,0,0])hole(height, hole_radius);
    }
}

module middle_piece(height, lenght, width){
    cube([lenght, width, height], true);
}


module flexlink() {
    union(){
        translate([shift,0,0])end_piece(height, length, width);
        translate([-shift,0,0])end_piece(height, length, width);
        middle_piece(height_middle, length_middle, width_middle);
    }
}
