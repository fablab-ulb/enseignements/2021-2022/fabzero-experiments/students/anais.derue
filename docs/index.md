# Welcome ! ☀️

Hello and welcome to the best blog for the FabZero-Experiments Course ! Here I will document everything I learn during this course, and the progress I make in my project

## About me

![](images/me-small2.jpg)

Hi ! I am Anaïs. I am a 20-year-old engineering student in electronics and computer science, and also a beginner web developper. I love experimenting with different technologies such as 3d printing and arduino to create fun useless little things. With this course, I want to learn more techniques in order to make more ambitious projects

## Studies

As I said earlier, I study electronics and computer science. This means I'm pretty much learning how to build a computer form scratch... in theory. My classes don't include much manual work, that's also part of why I follow this class. I'm currently in Erasmus in Brussels, at the ULB _(Free University of Brussels)_. I'm from France and I'm doing my degree at INSA _(National Institute of Science and Technology)_, in Rennes. Rennes is a city located in Brittany, in the west of France. Regional specialities includes cider and crêpes, truely it is heaven on earth.

## Hobbies

When I'm not coding, you can find me rock climbing, skateboarding or even slacklining when it's sunny enough. I also love to hang out in music festivals.

![](images/slack-small.jpg)
