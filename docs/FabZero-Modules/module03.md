# 3. Computer Controlled Cutting

This week we learned about laser cutting. We learned how to design 2d CAD files, and how to use the laser cutters avaliables in the fablab.

## Safety first !

Laser cutters are cool. They are also pretty dangerous, which is why we learned about a few important rules before using them. Here is a small recap :

- stay close to the machine when in use
- turn on the pressurized air
- turn on the fume extractor
- locate the emergency stop button
- locate the extinguisher and the fire blanket
- don't cut forbidden materials
- don't burn down the fablab

We also need to check if the material we want to cut through is allowed [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)

## Using the laser cutter

We formed groups of 5 and each group got to work on one of the laser cutters. My group worked on the **full spectrum muse**.
This laser cutter has a 50x30 cm cutting surface, and a max height of 6cm. The power of its laser is 40 W. To use it, we first need to connect to the machine's wifi and then go to http://fsl.local. This gives us access to **Retina Engrave** : the control interface. From there we can either upload a file or create one. We can use vector or matrice files. We can tell the machine how we want the cutting to be done via colors. We associate each part of the drawing a color and we associate each color to a particular speed and power.
We want to be as fast as possible, so we usually set the speed to max and ajust the power to get the desired depth of cut. If the max power isn't enough to cut as deep as we want, we start decreasing the speed.

## Making a reference sheet

Great, we now know how to use the laser cutter, but we still don't know what parameters we should use to get what we want. Obviously, if we use the same power to cut through paper and wood, the results will differ a bit. What we want to do is to make a universal test sheet that we can use on every material to test them. This avoid wasting time and energy on testing different parameters with our actual project.

This type of sheet already exists, they are well designed and widely used. However, we didn't want to use one of those, and instead wanted to make our own, beacause that's the ✨ maker spirit ✨. We used the machine's software to create our sheet.

![](../images/laser1.png)

And _boom_ in a few minutes our sheet is done, and a few minutes later the cutting was also done. Here is the final result :

![](../images/IMG-9646.jpg)

We're very happy with the results, this will help us with future projects.

We also wanted to try importing a file instead of creating it inside the software. Also we wanted to cut through another material : cork.
This is what we made :

![](../images/IMG-9647.jpg)

## Making a kirigami

This week's assignment is to make a [kirigami](https://en.wikipedia.org/wiki/Kirigami), which is basically a 3d object that you construct by cutting and folding cardboard or paper.

The kirigami I want to make is a [D20](https://en.wikipedia.org/wiki/D20_System) dice.
To draw the patron I need to use [Inkscape](https://inkscape.org/fr/), which is a software to make vector drawings.
I used the polygon tool to draw my triangles, and the line tool to make the contour red, so that I can tell the laser cutter to cut those lines and not engrave them like the others.

This is what the patron I designed looks like on Inkscape

![](../images/d20.png)

You can find this file [on my gitlab](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/anais.derue/-/blob/main/files/d20.svg)
