# 6. Electronics 2 - Production

Last week we learned how to use an arduino. You know what's better than using an arduino ? Building one ! So that's what we did this week. Well not exactly an arduino, but we built a microcontroller development board, and its core is even more powerful than the one in the arduino uno ! Our board is based on an Amtel samd11c, which has a 32-bit microprocessor (the arduino uno had a 8-bit one), however it only has 16kB of program memory.

## Soldering

The biggest part of our work this week was to solder every component on the board.

This is what the board looks like naked :

![](../images/before.jpg)

We started with the microcontroller, and it was by far the most difficult soldering I hever had to do, because the pins were so tiny.
We hade to double-check the orientation of the microcontrolor before soldering anything.

To make things easier, we were told to start with putting solder on two opposite pads (in diagonal), and then solder the two pins that go on these pads. However, I was stupid and did not follow this advice. I started buy putting solder one every pad, thinking it would be easier afterwards.

![](../images/mistake.jpg)

I was wrong. The bumps of solder everywhere made it very hard to place my microcontroller correctly, as you can see here :

![](../images/chip0.jpg)

After 45 minutes of battle, I eventually made it work !

![](../images/chip1.jpg)

It was now time to solder everything else ! Luckily, the other parts were easier to solder as thir pads were bigger.

At this point I was convinced I already burnt my chip because of how long it took me to solder everything, but here it was, really ugly, but finally done !

![](../images/boardfinished.jpg)

I made the mistake of inverting two of the resistors so I had to de-solder them and place them correctly after.

## Programming

Now was the moment of truth, will my board boot or did I wasted 3 hours of soldering ?

...

...

...

Aaaaand it worked ! I had never been so happy to see a little red led light up my entire life.

![](../images/boardworks.jpg)

Now I needed to program it.

To do that, we used the arduino IDE, that we configured so it could handle our board.
We had to add this url

     https://www.mattairtech.com/software/arduino/package_MattairTech_index.json

to the external boards manager, and then install it in the board manager.

![](../images/addBoard.png)

Then, we had to add the MattairTech SAM package into our IDE

![](../images/addBoard2.png)

Our board now shows up un the list of avaliable boards !

![](../images/addBoard3.png)

For our board to work properly, we had to make sure to select these parameters :

- Board : Generic D11C14A

- Clock : INTERNAL_USB_CALIBRATED_OSCILLATOR

- USB-CONFIG : CDC_ONLY

- SERIAL_CONFIG : ONE_UART_ONE_WIRE_NO_SPI

- BOOTLOADER : 4kB Bootloader

Now, I simply used a led blink example to make the green led that is already on the board light up

![](../images/blink.mp4)

Here is the code I used to make the led link :

```

/*
  Blink

  Turns the built-in LED on for half a second, then off for one second, repeatedly.

  FILE   : blink

  AUTHOR : Anaïs Derue <anais.derue@ulb.be>

  DATE   : 2022-04-05

  LICENSE : Creative Commons Attribution 4.0 International (CC BY 4.0) [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).


*/
int led = 15;
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin led as an output.
  pinMode(led, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for half a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for half a second
}
```
