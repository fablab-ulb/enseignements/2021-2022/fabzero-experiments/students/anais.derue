# Final Project

For my final project, I will use the Challenge-based learning framework to define a problem and then develop a frugal solution using the fablab.

## Challenge-based learning framework

### Engage - Find a challenge that motivates you

**Big ideas** - _What is the broad theme or concept I would like to explore ?_

For this project, I want to find a way to recycle PLA waste from 3d printers

**Essential questions** - _What are the essential questions that reflect personal interests and the needs of the community ?_

How can we turn the plastic waste into something reusable ?

**Challenge** - _what is your call for action ?_

As I own a 3d printer, I want a solution that is cheap and easily reproducible at home, so I can recycle my PLA waste myself

### Investigate - step on the shoulders of the giants not on their toes

**Guiding questions** - _what are all the questions that needs to be answered in order to tackle the challenge ? Priority ?_

What is the first thing I need to do with the PLA waste ? Should I crush it or melt it ?
Am I trying to make a new spool to 3d print more stuff or am I trying to make an object ?
If I want to make a spool -> how can I turn the plastic into a nice filament with the right diameter ?
If I want to make an object -> Should I use a mould ? What kind of object ?
Can I make something else with the PLA ?

**Guiding activities/ressources** - _What resources can I use to answer those questions ? Science._

A student from last year's FabZero course, Simone Vitale, already did some research on this issue. [His documentation](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/simone.vitale/project/shredding/) is a really good start to avoid the mistakes he did in the beginning.

[Precious Plastic](https://community.preciousplastic.com/academy/intro) makes machines to recycle PLA waste, their website is a gold mine of info abous plastic recycling

A few other websites such as [All3dp](https://all3dp.com/2/is-pla-recyclable/) or [3dSolved](https://3dsolved.com/is-pla-recyclable/) also provide a few useful info about the subject

**Analysis and synthesis** - _write a summary of your findings, facts and data collected_

After reading quite a lot of info, I eliminated the option of making my own filament, as it is very difficult to make, it costs a bit of money, and the quality of the filament will be mediocre, resulting in more fails and therefore more waste.

I also dropped the idea of making an object because that would require making a mould, which is not easy, plus that would make my solution only useful for the people that desire to make that one object.

I found that PLA can be melted at 190°C, and can be shredded with a blender (if already small enough).
It also can be cut without risk with a laser cutter.

I talked with [Benjamin](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/benjamin.hainaut/) about the problem and we decided to work together since he also own a 3d printer and is facing the same issue. We concluded that the best solution would be to melt the PLA and press it while it's hot to make thin plastic sheets that we can cut with the laser cutter to make new objects. I already know that I want to make a set of coasters with it.

### Act - Develop a solution, implement it and get feedback

**Solution concepts** - _What is your solution about ?_

Our solution is to make a small press, to turn melted plastic into thin, reasy-to-use, plastic sheets. We will melt the PLA at 190°C, then quickly put it in the press to make the little air bubbles go away. After that, the plastic sheet will cool down and be ready to be cut into a new thing !

**Solution development** - _how do this solution solve the challenge ? Prototype and develop._

This solution will solve the challenge because instead of having lots of tiny bits of plastic, we will have plastic sheets that can be easily cut with a laser.

**Implementation and evaluation** - _Experiment and evaluate the solution._

## Position along the 17 Sustainable Development Goals listed by the United Nations

This project will contribute to reduce plastic waste, and therefore position itself among goal #12 of the 17 sustainable development goals listed by the UN

![](./images/projet/SDG_report_2021_Goal%2012.png)

## Conception of the project

Here is the plan :

![](./images/projet/project.jpg)

The rest of the documentation will be in french, as it is co-written with Benjamin.

### L'idée de base

L'idée de base peut être séparée, un peu comme une recette de cuisine, en plusieurs étapes :

- Récolte du plastique
- Préparation du plastique
- Fonte du plastique
- Moulage et traitement du plastique
- Utilisation du plastique

#### Récolte du plastique

Cette étape est sans doutes la plus simple, étant donné que c'est notre problématique. En effet, que ce soit chez nous ou au fablab, nous avons une source qui, même si on ne le veut pas, se remplit de tous les supports et erreurs de nos impressions.

#### Préparation du plastique

On peut entendre trois choses par "Préparation du plastique".
La première est la forme sous laquelle on va mettre le plastique. Les [modèle traditionnel de machine servant à recycler le plastique sous forme de plaques](https://community.preciousplastic.com/academy/build/sheetpress) demandent en général que le plastique d'entrée soit sous forme de copeaux. On verra par la suite que ce n'est pas forcément une nécéssité, mais que cela peut aider à l'homogénéisation des plaques obtenues. Nous avons décidé dans un premier temps de ne pas réduire en copeaux nos déchets de plastique.

La deuxième forme de préparation que l'on peut envisager est plus estétique. Je parle ici d'un tri des couleurs de plastique que l'on va utiliser, soit de manière monochrome, soit avec des associations de couleurs qui rendent bien ensemble.

La dernière forme de préparation est la plus importante, un tri par type de plastique. Le plastique le plus courant au fablab et chez les particuliers est le PLA, mais d'autres plastique peuvent aussi être utilisés. Il est important pour nous de bien séparer ces différents plastiques, ceux-ci n'ayants pas les mêmes caractéristiques.

#### Fonte du plastique

Nous allons nous concentrer sur le PLA, qui est à notre portée le plus abondant. Comme on le sait, dans les imprimantes 3D, le PLA est amené à une température de maximum 220°C, mais le PLA commence déjà à fondre dès 175°C.

Afin de limiter le risque de brûler les plus fins des bouts de PLA, nous allons limiter la température à 200°C.

Pour faire chauffer notre plastique, nous allons simplement le placer dans un four, sur une surface la moins adhérente possible, du papier sulfurisé par exemple.

#### Moulage et traitement du plastique

Une fois le plastique chaud, la course contre la montre commence. On a moins d'une minute avant que le plastique ne soit plus maléable, il ne faut pas trainer.

L'idée est de placer le plastique fondu dans un moule, que l'on va fermer par une positif du moule et que l'on va presser. Le moule a pour objectif de définir la forme de notre plaque, et la pression est là pour deux raisons. La première est d'uniformiser l'épaisseur de la plaque, la deuxième est de limiter la présence d'air dans la plaque.

#### Utilisation du plastique

Une fois notre plaque obtenue, elle peut servir de matière première. Certaines applications l'utilisent comme telle, pour par exemple faire des tuiles de toit. L'application que nous visons est l'utilisation des plaques avec une découpeuse laser, comme on utiliserait une plaque de plexiglass.

### La preuve de concept

#### La préparation de la plaque

Avant de se lancer dans la construction d'une réelle presse et de passer à une vraie mise en pratique, nous avons fait un petit essai pour vérifier la faisabilité de notre projet.

Pour ce faire, nous avons pris une petite poignée de reste de PLA.

![](./images/projet/PLA_non_fondu.jpg)

Après un premier passage au four à 220°C pendant deux minutes suivi d'un passage au rouleau à patisserie, deux conclusions faciles se sont impossées.
Premièrement, 220°C est une température trop élevée, en effet, les plus fins filaments de PLA ont brulé au lieu de fondre.
Deuxièmement, 2 minutes n'est pas suffisant pour homogénéiser suffisament le PLA avec des pièces assez grosses encore présentes.

![](./images/projet/PLA_premier_passage.jpg)

Un second passage fût donc nécéssaire afin de bien tout homogénéiser.
Après un nouveau passage sous le rouleau à patisserie, une plaque relativement homogène est obtenue. On voit donc qu'une pression relativement faible est déjà suffisante pour avoir un résultat convenable. La plaque obtenue a une épaisseur légèrement variable, mais le plastique est assez homogène.

![](./images/projet/PLA_resultat.jpg)

#### L'utilisation de la plaque.

Maintenant que nous avons une plaque, nous avons voulu voir ce qui pouvait en être fait.

Pour ce faire, nous avons téléchargé un simple logo du FabLab, et nous avons essayé de le graver à la découpeuse laser, avant de le découper de la plaque.
La plaque étant relativement épaisse, et la découpeuse utilisée étant la plus faible, plusieurs passages ont été nécéssaire, mais le résultat final est plus que correct.

![](./images/projet/plastic_engraved.jpg)

Les utilisations de ces plaques sont donc infinies et limitée seulement par l'imagination de tout un chacun !

## La mise en pratique finale

Maintenant que nous savons que tout fonctionne, il est grand temps se lancer dans la conception d'un prototype complet et fonctionnel !

Notre source de plastique sera les bacs de tri de salle des imprimantes du FabLab ULB qui sont aujourd'hui plus que bien remplis.

Pour chauffer notre plastique à 200°C, nous allons utiliser le four présent au FabLab qui, ça tombe bien, peut chauffer jusqu'à 205°C.

Pour la presse, nous allons la faire nous même, en construisant un système à base de structure solide en bois et de cric de voiture.

### Le four

Le four présent au fablab est simple d'utilisation. Il suffit d'allumer la ventilation avec l'interrupteur vert puis d'allumer le four avec l'interrupteur rouge (de toute façon le four ne s'allume pas si la ventilation est éteinte).

![](./images/projet/oven.jpg)

Nous avons trouvé au fablab une plaque de four dans laquelle mettre notre papier cuisson et nos déchets de plastique.
Nous avons également trouvé une paire de gants résistants a la chaleur pour manipuler cette plaque sans danger.

Nous sommes maintenant prêts a faire fondre notre plastique.

![](./images/projet/before_oven.jpg)

La porte du four ne fermant pas très bien et le volume du four étant particulièrement grand, le four a mis une heure et demie à atteindre les 200°C. Heureusement, nous avions mis le plastique a l'intérieur des le debut, il a donc pu commencer a fondre des les 175°C.
Nous avons sorti rapidement le plastique une fois les 200°C atteints car nous avions plus beaucoup de temps disponible.
Nous l'avons alors immédiatement mis dans la presse.

### La presse

Nous avons fabriqué une presse en bois à partir d'une presse déjà présente au FabLab, qu'il a fallu remettre en état.
Son principe est simple, on met ce que l'on souhaite presser entre les deux plaques de bois horizontales, et on vient faire pression dessus à l'aide du cric.

![](./images/projet/press2.jpg)

### Le moule

Entre les deux planches horizontales de la presse se trouve un moule, qui permet de faire des feuilles de plastique bien homogènes. Nous avons fabriqué ce moule en découpant un rectangle dans une planche de bois à la découpeuse laser.L'exterieur de la découpe a servi a a faire les contours du moule, et l'interieur sert a faire le positif, positionné sur la plaque supérieure de la presse.

![](./images/projet/wood.jpg)
![](./images/projet/press.jpg)

### Le résultat

Après avoir mis notre plastique fondu dans la presse et fait pression, nous avons obtenu ce résultat :

![](./images/projet/after_oven.jpg)

Nous n'avons pas réussi a obtenir une plaque, parce que nous n'avons pas mis assez de plastique dans le four. Cependant le plastique a quand même bien fondu, et est bien aplati, donc on imagine qu'en en mettant une plus grande quantité dans le four ça peut marcher.

## BOM - Bill of Materials

_Rapide estimation en un tableau récapitulatif des couts de notre implémentation si on devait la refaire de zéro._

Pour ce projet, un des objectifs était de ne rien dépenser. Il a été atteint car nous avons réussi à n'utiliser que des matériaux de récupération. Cependant, sans avoir accès a ces matériaux, voici la BOM (Bill of materials = liste des couts) :

| Objet                                                                                                                                                                                                                                                                                         | Cout             |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------- |
| [Cric](https://www.cdiscount.com/auto/outillage-atelier/cric-hydraulique-de-bouteille-de-2/f-1330501-pri8434852165778.html?idOffre=401490950#cm_rr=FP:7583423:SP:CAR)                                                                                                                         | 22€              |
| [Bois pour le moule](https://www.leroymerlin.fr/produits/menuiserie/panneau-bois-tablette-etagere-tasseau-moulure-et-plinthe/panneau-bois-agglomere-mdf/panneau-contreplaque-okoume-twin-1200-x-400-x-10-mm-88105325.html)                                                                    | 18.30€           |
| [Planches de bois pour l'armature](https://www.laboutiquedubois.com/avives-douglas-bruts-certifie-pefc-100-679.html)                                                                                                                                                                          | 33.88€ (pour 4)  |
| [Planches pour faire les plaques horizontales](https://www.leroymerlin.fr/produits/menuiserie/panneau-bois-tablette-etagere-tasseau-moulure-et-plinthe/tasseau-planche-et-equerre-de-fixation/planche-et-latte-a-sommier/planche-sapin-petits-noeuds-brut-25-x-150-mm-l-2-4-m-68189520.html#) | 77.40 € (pour 6) |
| [vis](https://www.leroymerlin.fr/produits/quincaillerie/cheville-vis-clou-et-boulon/vis/vis-a-bois/lot-de-35-vis-acier-tete-fraisee-standers-diam-4-mm-x-l-16-mm-82231803.html)                                                                                                               | 3.30€            |
| [Papier sulfurisé](https://www.cdiscount.com/au-quotidien/hygiene-soin-beaute/papier-de-cuisson-15x0-30-m/f-127020211-sch3264221612863.html#mpos=0cd)                                                                                                                                         | 5.45€            |
| TOTAL                                                                                                                                                                                                                                                                                         | 160.33€          |

Le total est élevé mais la plupart de ces matériaux peuvent être trouvés en récupération assez facilement.

## Amélioration

Un des problèmes majeur de notre presse est qu'elle ne peut pas s'utiliser seul. La faire plus légère serait une belle amélioration, pour pouvoir soulever la partie haute d'une main et mettre le plastique a l'interieur de l'autre main.

Un problème que nous avons rencontré est que le four ne comporte pas de vitre, et il était donc impossible de voir a quel point notre plastique avait fondu avant de le sortir.

# Informations additionnelles

## Les déchets d'impression 3D

L'impression 3D est un outil génial permettant un prototypage rapide et la création d'objets facilement pour toutes et tous chez soi. Malheureusement, comme tout, il y a un revers à la médaille.

La majorité des imprimantes 3D utilisent du plastique, hors [la production de plastique mondiale ne fait qu'augmenter d'année en année](https://plasticoceans.org/the-facts/) et approche les 400 millions de tonne en 2020 . Bien entendu, l'impression 3D n'est pas la seule responsable dans cet océan de plastique, mais représente malgré tout près de [20 000 tonnes de plastique](https://mdpi-res.com/d_attachment/polymers/polymers-13-00744/article_deploy/polymers-13-00744-v2.pdf?version=1614740536).
Les estimations indique que dans ces 20 000 tonnes, 5000 tonnes sont destinés à être des déchets.

Certaines [régions du monde ont une très bonne gestions de leurs déchets plastique](https://www.umsicht.fraunhofer.de/content/dam/umsicht/en/documents/publications/2017/pla-in-the-waste-stream.pdf), mais ce n'est bien entendu pas encore une généralité. Il y a donc une opportunité à dévelloper des méthodes de recyclage à plus petites échèlles et simple à mettre en place à peu près n'importe où.

![](/images/projet/recycling.png)


## [Les propriétés du PLA](https://fr.wikipedia.org/wiki/Acide_polylactique)

* Le PLA (acide polylactique) est un polymère biodégradable, souvent obtenu à base de maïs. 
* En dessous de 60°C, le PLA est totalement amorphe.
* A partir de 60°C, le PLA commence à pouvoir être facilement composté.
* A partir de 175°C, le PLA atteint sa temmpérature de fusion, la température qui nous intéresse. 


* Tout comme l'acier, le PLA peut adopter différentes structure crystaline en fonction des températures auquelles il refroidi, et donc avoir des propriétés mécanique différentes.





